###
### (C) 2014 by DMA 
### Main class. Starts everything
###
from base import Base
from temperature import Temperature
from relay import Relay
from calculation import Calculation

import time
import logging

class Main(Base):

    def __init__(self):
        Base.__init__(self)
         
        temperature = Temperature()
        relay = Relay()
        calculation = Calculation()
        
        logging.debug("Main module loaded")
        logging.debug("Starting loop")
        
        cancel=False
        
        ## endless loop till cancel is 1
        while(not cancel):
            
            if(calculation.getHeatingMode() == 0):
                relay.offIn2()
            else:
                relay.onIn2()
                
            time.sleep(3*60)
        
        
main = Main()