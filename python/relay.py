###
### (C) 2014 by DMA 
### Controls the Relais for switching on the gas-boiler
###
import RPi.GPIO as GPIO
import time
import logging
import logging.config
from config import Config
from base import Base

# For Foxnovo 5V 2-Kanal-Relay-Modul-Shield for Arduino ARM PIC AVR DSP (amazon)
# Productumber: 13991725037
# .         .       .      .  .  .  .  
#JD-VCC    VCC     GND
#               _
#   . JD-VCC     |  Bridge/Jumper
#   . VCC       _|
#   . GND
#
#
#   . GND   GND (raspi)
#   . IN1   GPIO ( LOW=On, HIGH=Off )
#   . IN2   GPIO ( LOW=On, HIGH=Off )
#   . VCC   5V (raspi)
#
#   if IN1 or IN2 set to LOW, the relay 1 or 2 will work
#   thats it


class Relay(Base):
    
    def __init__(self):
        Base.__init__(self)
        
        global IN1, IN2, config
        config = Config()
        
        IN1 = int(config.getConfig("relais", "in1"))
        IN2 = int(config.getConfig("relais", "in2"))
        print(IN2)
        # RPi.GPIO Layout verwenden (wie Pin-Nummern)
        GPIO.setmode(GPIO.BOARD)
        logging.debug("set GPIO Mode to GPIO.BOARD")
    
        # set to output for IN1 
        GPIO.setup(IN1, GPIO.OUT)
        logging.debug("set IN1(PIN " + str(IN1) + ") to  GPIO.OUT")
        
        # set to output for IN2 
        GPIO.setup(IN2, GPIO.OUT)
        logging.debug("set IN2(PIN " + str(IN2) + ") to  GPIO.OUT")
        
        # disable IN1
        self.offIn1()
        
        # disable IN2
        self.offIn2()
        

    # switch on IN1
    def onIn1(self):
        GPIO.output(IN1, GPIO.LOW)
        logging.debug("enable IN1")
    
    #switch off IN1
    def offIn1(self):
        GPIO.output(IN1, GPIO.HIGH)
        logging.debug("disable IN1")
    
    #switch on IN2
    def onIn2(self):
        GPIO.output(IN2, GPIO.LOW)
        logging.debug("enable IN2")
    
    #switch off IN2
    def offIn2(self):
        GPIO.output(IN2, GPIO.HIGH)
        logging.debug("disable IN2")
    
    
    
    ##
    # TESTING
    ##
    def testIn1(self):
        self.onIn1()
        time.sleep(0.5)
        self.offIn1()
    
    def testIn2(self):
        self.onIn2()
        time.sleep(0.5)
        self.offIn2()

#relay = Relay()
#relay.testIn1()
#relay.testIn2()