###
### (C) 2014 by DMA 
### All Methods for temperature controling
###
from w1thermsensor import W1ThermSensor
from config import Config
from base import Base

import time
import logging


class Temperature(Base):

    config=Config()
    sensor=W1ThermSensor()
    
    def __init__(self):
        global tempDelta, tempMax, tempMin, logging
        
        Base.__init__(self)
        logging.debug("Temperature module loaded")

    ##
    # GET/SET
    ##
    
    ## tempMax
    def getMaxTemperature(self):
        return self.config.getConfig("temperature", "max_temperature")
        
    def setMaxTemperature(self, maxTemperature):
        self.config.setConfig("temperature", "max_temperature", maxTemperature)
        logging.debug("set max temperature to " + str(maxTemperature))
    
    
    ## tempMin<
    def getMinTemperature(self):
        return self.config.getConfig("temperature", "min_temperature")
        
    def setMinTemperature(self, minTemperature):
        global tempMin
        self.config.setConfig("temperature", "min_temperature", minTemperature)
        logging.debug("set min temperature to " + str(minTemperature))
    
    
    ## tempDelta
    def getDeltaTemperature(self):
        return self.config.getConfig("temperature", "delta_temperature")
    
    def setDeltaTemperature(self, deltaTemperature):
        global tempDelta
        self.config.setConfig("temperature", "delta_temperature", deltaTemperature)
        logging.debug("set delta temperature to " + str(deltaTemperature))
        
    ## room temperature from sensor
    def getTemperature(self):
        return self.sensor.get_temperature()
    
    ## room temperatrure from sensor with added delta
    def getTemperatureWithDelta(self):
        return self.sensor.get_temperature()+self.getDeltaTemperature()
    
    
    def testMinTemp(self):
        logging.debug("get min temp: " + str(self.getMinTemperature()))
        self.setMinTemperature(42.00)
        logging.debug("get new min temp: " + str(self.getMinTemperature()))
    
    def testMaxTemp(self):
        logging.debug("get max temp: " + str(self.getMaxTemperature()))
        self.setMaxTemperature(42.00)
        logging.debug("get new min temp: " + str(self.getMaxTemperature()))
    
    def testDeltaTemp(self):
        logging.debug("get delta temp: " + str(self.getDeltaTemperature()))
        self.setDeltaTemperature(2.00)
        logging.debug("get new delta temp: " + str(self.getDeltaTemperature()))
    
    def testSensor(self):
        logging.debug("get room temperature 1 from sensor: " + str(self.getTemperature()) + " wait ...")
        time.sleep(2)
        logging.debug("get room temperature 2 from sensor: " + str(self.getTemperature()) + " wait ...")
        time.sleep(2)
        logging.debug("get room temperature 3 from sensor: " + str(self.getTemperature()) + " ok")
    
    def testAll(self):
        self.testMinTemp()
        self.testMaxTemp()
        self.testDeltaTemp()
        self.testSensor()
        
#temperature = Temperature()
#temperature.testAll()
#temperature.testSensor()