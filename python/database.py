###
### (C) 2014 by DMA 
### Database singleton class
###
from base import Base

import sys
import logging
import MySQLdb
 

class Database():
    db = MySQLdb.connect(host="localhost", user="root", passwd="rootpw", db="thermopi")
 
    def __init__(self):
        pass
    
    def getCon(self):
        return self.db
    
    ## query helper
    def queryDb(self, sql):
        
        try:    
            cur = self.db.cursor()
            cur.execute(sql)
            self.db.commit()
            
            retval = cur.fetchall()
            if(retval):
                return retval
                
        except self.db.Error, e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            sys.exit(1)
            
    
        return False