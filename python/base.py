###
### (C) 2014 by DMA 
### Base class for things like initialising the logger and so on
###
import time
import logging
import logging.config



class Base():
    
    def __init__(self):
        logging.config.fileConfig('logging.conf')
        logging.debug("Logger module loaded")
