###
### (C) 2014 by DMA 
### Config class. Getter/Setter for mysql based configuration
###
import MySQLdb as mdb
import sys
import logging
from base import Base

from temperature import Temperature
from statistic import Statistic
from config import Config
from date import Date

class Calculation(Base):

    stat = Statistic()
    date = Date()
    
    def __init__(self):
        global temperature, logging, config, date
        global maxTemperture, minTemperature, deltaTemperature, temperatureOverheating, roomTemperature
        Base.__init__(self)
        config = Config()
        temperature = Temperature()
        
        logging.info("Calculation module loaded")


    def getHeatingMode(self):
        global maxTemperture, minTemperature, deltaTemperature, temperatureOverheating, roomTemperature
             
        temperatureOverheating=0
        roomTemperature = float(temperature.getTemperature())
        maxTemperature = float(temperature.getMaxTemperature())
        minTemperature = float(temperature.getMinTemperature())
        deltaTemperature = float(temperature.getDeltaTemperature())
        
        maxDelta1 = float(config.getConfig("calculation", "delta_max_temperature_1"))
        maxDelta2 = float(config.getConfig("calculation", "delta_max_temperature_2"))
        maxDelta = (maxDelta1 + maxDelta2) / 2
        
        minDelta1 = float(config.getConfig("calculation", "delta_min_temperature_1"))
        minDelta2 = float(config.getConfig("calculation", "delta_min_temperature_2"))
        minDelta = (minDelta1 + minDelta2) / 2
        
        theTemperature = 17
        # + because, always adding all data is the best way
        # for maxTemperature, delta must be a negative float  (e.g. -1.25)
        logging.info((roomTemperature + maxDelta) + deltaTemperature)
        
        # if it is sleep time, set the requesting time to min temperature
        # else set the temperature to max temperature
        if(self.date.inOffTime() == True):
            theTemperature = minTemperature
            logging.info("night mode")
        else:
            theTemperature = maxTemperature
            logging.info("day mode")
        
        
        if((roomTemperature + maxDelta) + deltaTemperature >= theTemperature):
            if(temperatureOverheating > roomTemperature):
                self.saveOverheating(roomTemperature)
            else:
                self.setOverheating(roomTemperature)
            
            self.stat.writeData(0)
            return 0
        elif((roomTemperature + minDelta) + deltaTemperature <= theTemperature):
            return 1
            self.stat.writeData(1)
        else:
            self.stat.writeData(0)
                
                
            


    # toggle values to have a semi history for mean value calculation
    def saveMaxDelta(self, maxDelta):
        config.setConfig("calculation", "delta_max_temperature_2", config.getConfig("calculation", "delta_max_temperature_1"))
        config.setConfig("calculation", "delta_max_temperature_1", maxDelta)
    
    # toggle values to have a semi history for mean value calculation
    def saveMinDelta(self, minDelta):
        config.setConfig("calculation", "delta_min_temperature_2", config.getConfig("calculation", "delta_min_temperature_1"))
        config.setConfig("calculation", "delta_min_temperature_1", minDelta)


    def setOverheating(self, temperature):
        global temperatureOverheating
        temperatureOverheating = temperature
        logging.info("startMaxDelta")
        
    def saveOverheating(self, temperature):
        # set MaxDelta. (not substrating, adding; thats why its calculating that way!)
        self.saveMaxDelta((maxTemperature - temperatureOverheating))
        logging.info("stoptMaxDelta")
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ##
    ## moeglichkeit 1:
    ## ein max und ein min delta zur anpassung des heizzeitpunktes.
    ## delta wird ermittelt, indem die temperatur zwischen maxTemperatur und maximal gestiegener temperatur ermittelt wird.
    ## dieser wird dann zu der max und min temperatur addiert.
    ## der wert muss beim max negativ und beim min positiv sein
    ##
    
    ##
    ## moeglichkeit 2:
    ## eine statistische auswertung der heizkurven
    ## 1. starten eines timers, wenn die temperatur erreicht ist 
    ##    solange diese steigt, muss der timer weiter laufen
    ## 2. stoppen des timers, wenn die temperatur wieder faellt
    ## 3. das zeitliche delta muss bei der heiz zeit abgezogen werden
    ##
    ## problem: man muss messen, wie lange man fuer z.B. einen grad temperatur braucht.
    ## nicht so optimal, aber am genauesten
    ##
    
    
    ##
    ## moeglichkeit 3:
    ## 
    