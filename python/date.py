###
### (C) 2014 by DMA 
### Date class
###
from base import Base
import time
import datetime
import logging
from config import Config

class Date(Base):
    
    def __init__(self):
        Base.__init__(self)
        config = Config()
        
    def inOffTime(self):
        config = Config()
        now = datetime.datetime.now()
        nightbegin = now.replace(hour=23, minute=30, second=0, microsecond=0)
        nightend = now.replace(hour=07, minute=00, second=0, microsecond=0)
        
        #sleepTimeBegin = config.getConfig("sleepTimeBegin")
        #sleepTimeEnd = config.getConfig("sleepTimeEnd")
        
        #nightbegin = now.replace(hour=sleepTimeBegin, minute=00, second=0, microsecond=0)
        #nightend = now.replace(hour=sleepTimeEnd, minute=00, second=0, microsecond=0)
        
        #logging.info("config.sleepTimeBegin = " + sleepTimeBegin)
        #logging.info("config.sleepTimeEnd = " + nightend.isoformat())
         
         
        if(now > nightbegin or now < nightend):
            return True
        
        return False
        
