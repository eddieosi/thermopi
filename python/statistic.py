###
### (C) 2014 by DMA 
### Statistics class. Writes all data to DB
###
from base import Base
from database import Database
from temperature import Temperature

import time
import logging


class Statistic(Base):
    temp = Temperature()
    db = Database()
    
    def __init__(self):
        Base.__init__(self)    
        con = self.db.getCon()
         
    def writeData(self, heating):
        if(heating == True):
            heating = 1
        elif(heating == False):
            heating = 0
        
        sql = "INSERT INTO `statistic` (`temperature`, `heating`, `max_temperaure`, `min_temperature`, `delta_temperature`) VALUES('%s', '%s', '%s', '%s', '%s')" %(self.temp.getTemperature(), heating, self.temp.getMaxTemperature(), self.temp.getMinTemperature(), self.temp.getDeltaTemperature() )
        
        logging.debug(sql)
        self.db.queryDb(sql)
        
def test():
    stat = Statistic()
    
    for x in range(0, 100):
        
        stat.writeData(1)
        time.sleep(1)
        stat.writeData(1)
        time.sleep(1)
        stat.writeData(0)
        time.sleep(1)
        stat.writeData(0)
    
    
#test()
        