###
### (C) 2014 by DMA 
### Config class. Getter/Setter for mysql based configuration
###
import MySQLdb as mdb
import sys
import logging
from base import Base
from database import Database

class Config(Base):

    db=Database()

    def __init__(self):
        Base.__init__(self)
        global con, logging
        
        con = self.db.getCon()
        
        logging.debug("Config module loaded")
        
    ## get the config key for module, key combination (key is optional)
    def getConfig(self, module, key=False):
        
        if(key == False):
            sql = "SELECT `value` FROM `config` WHERE `module`='%s'" %(module)
        else:
            sql = "SELECT `value` FROM `config` WHERE `module`='%s' AND `key`='%s'" %(module,key)
            
        logging.debug(sql)
        
        if(key == False):
            return self.db.queryDb(sql)
        else:
            return self.db.queryDb(sql)[0][0]
    
    def setConfig(self, module, key, value):
        if(self.getConfig(module, key)):
            ## update key
            sql = "UPDATE config SET `value`='%s' WHERE `module`='%s' AND `key`='%s' " %(value,module,key)
        else:
            ## insert key
            sql = "INSERT INTO `config` (`module`, `key`, `value`) VALUES('%s', '%s', '%s')" %(module,key,value)
    
        logging.debug(sql)
        
        return self.db.queryDb(sql)
        
        
    
        
        
def test():
    config = Config()
    stat = Statistic()
    logging.debug(config.getConfig("temperature", "max_temperature"))
    #logging.debug(config.getConfig("temperature"))
    #logging.debug("setConfig")
    #config.setConfig("temperature", "delta_temperature", "1.00")
    #logging.debug(config.getConfig("temperature"))


#test()