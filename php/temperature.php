<?php

include_once("database.php");

class Temperature{
    
    private $db;
    
    function __construct() {
        $this->db = new Database();
    }
    
    public function getTemperature(){
        $sql = "SELECT temperature FROM statistic ORDER BY id DESC LIMIT 1;";
        $temperature = $this->db->queryDb($sql, 1);
        $temperature = $temperature[0]['temperature'];
        return $temperature;
    }
    public function getTemperatureWithDelta(){
        return $this->getTemperature() + $this->getDeltaTemperature();
    }
        
    
    public function getMaxTemperature(){
        $sql = "SELECT `value` FROM `config` WHERE `module`='temperature' AND `key`='max_temperature';";
        
        $temperature = $this->db->queryDb($sql, 1);
        $temperature = $temperature[0]['value'];
        return $temperature;
    }
    public function setMaxTemperature($maxTemperature){
        $sql = "UPDATE `config` SET `value` = " . $maxTemperature . " WHERE `module`='temperature' AND `key`='max_temperature';";
        $temperature = $this->db->queryDb($sql, 1);
    }
    
    public function getMinTemperature(){
        $sql = "SELECT `value` FROM `config` WHERE `module`='temperature' AND `key`='min_temperature';";
        
        $temperature = $this->db->queryDb($sql, 1);
        $temperature = $temperature[0]['value'];
        return $temperature;
    }
    public function setMinTemperature($minTemperature){
        $sql = "UPDATE `config` SET `value` = " . $minTemperature . " WHERE `module`='temperature' AND `key`='min_temperature';";
        $temperature = $this->db->queryDb($sql, 1);
    }
    
    public function getDeltaTemperature(){
        $sql = "SELECT `value` FROM `config` WHERE `module`='temperature' AND `key`='delta_temperature';";
        
        $temperature = $this->db->queryDb($sql, 1);
        $temperature = $temperature[0]['value'];
        return $temperature;
    }
    public function setDeltaTemperature($deltaTemperature){
        $sql = "UPDATE `config` SET `value` = " . $deltaTemperature . " WHERE `module`='temperature' AND `key`='delta_temperature';";
        $temperature = $this->db->queryDb($sql, 1);
    }
    
    public function isHeating(){
        if($this->getTemperatureWithDelta() < $this->getMaxTemperature()){
            return true;
        }else{
            return false;
        }
    }
}




?>