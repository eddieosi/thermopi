<?php

include_once("database.php");

class Statistic{

    private $db;
    
    function __construct() {
        $this->db = new Database();
    }
    
    public function getStats($from, $to=false){
        if($to !== false){
            $sql = "SELECT * FROM statistic WHERE unix_timestamp(date) BETWEEN '" . $from . "' AND '" . $to . "' ORDER BY id ASC ;";
        }else{
            $sql = "SELECT * FROM statistic WHERE unix_timestamp(date) > " . $from . " ORDER BY id ASC;";
        }
        //var_dump($sql);
        $stats = $this->db->queryDb($sql, 1);
        return $stats;
    }
    
    
    public function clearStatistic($code){
        if($code == "F8_157_03R_83S73"){
            $sql = "TRUNCATE TABLE statistic;";
            $stats = $this->db->queryDb($sql, 0);
        }
    }
}
?>