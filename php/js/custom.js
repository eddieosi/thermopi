
$(".max").knob({'release' : function(data){changeMaxTemperature(data)} });
$(".min").knob({'release' : function(data){changeMinTemperature(data)} });
$(".delta").knob({'release' : function(data){changeDeltaTemperature(data)} });







/**
 *  MAX TEMPERATURE
 */
function changeMaxTemperature( temp ){
    var theData =   {   "action": "max_temperature",
                        "value" : temp
                    }
    $.get("ajax.php", theData);
    $(".max_temperature_value").text(temp);
}


/**
 *  MIN TEMPERATURE
 */
function changeMinTemperature( temp ){
    var theData =   {   "action": "min_temperature",
                        "value" : temp
                    }
    $.get("ajax.php", theData);
    $(".min_temperature_value").text(temp);
}



/**
 *  DELTA TEMPERATURE
 */
function changeDeltaTemperature( temp ){
    var theData =   {   "action": "delta_temperature",
                        "value" : temp
                    }
    $.get("ajax.php", theData);
    $(".delta_temperature_value").text(temp);
}
    


/**
 *  REFRESH TEMPERATURE / HEATING
 */
 
setInterval(function(){ 
    $.get("ajax.php", {"action": "temperature"}, function( data ) {
        $( ".temperature" ).html( data );
    });
    
     $.get("ajax.php", {"action": "heating"}, function( data ) {
        $( ".heating" ).html( data );
    });
    
    
}, 5000);


/**
 *  STATISTICS FOR CHART
 */ 
 
 
 var today = (Date.today().getTime()/1000);
 //var yesterday = new Date().add(-1).day().getTime();
 //var today = new Date().getTime();
 console.log(today);
 $.getJSON("ajax.php", {"action": "stats", "from" : today}, function( data ) {
        doStats(data);
    });


function doStats(theData){
    
    new Morris.Area({
      element: 'thermochart',
      data: theData,
      xkey: 'hour',
      ykeys: ['value', 'heating'],
      labels: ['Temperatur', 'Heating'],
      ymin: 15.00,
      ymax: 24.00,
      resize: true,
      pointSize:0
    });
}