<?php

class Database{
    
    private $host = "localhost";
    private $user = "root";
    private $pass = "rootpw";
    private $db   = "thermopi";
    
    
    private function getConnection(){
        $con = mysql_connect($this->host, $this->user, $this->pass);
        mysql_select_db($this->db, $con);
        return $con;
    }
    
    public function queryDb($sql, $parseValue=1){
        $con = $this->getConnection();
        
        if($parseValue == 1){
            return $this->parseValues(mysql_query($sql, $con));
        }else{
            return mysql_query($sql, $con);
        }
    }
    
    private function parseValues($query){
        $retVal = array();
        
        while($res = mysql_fetch_assoc($query)){
            $retVal[] = $res;
        }
        
        return $retVal;
    }    
    
}