<?php

include_once("temperature.php");
include_once("statistic.php");
include_once("time.php");

if(isset($_GET['action']) && !empty($_GET['action']) ){
    
    $temp = new Temperature();
    $stat = new Statistic();
    $time = new Time();
    
    if(isset($_GET['value']) && !empty($_GET['value'])){
        switch($_GET['action']){
            case "max_temperature":
                $temp->setMaxTemperature($_GET['value']);
                break;
            case "min_temperature":
                $temp->setMinTemperature($_GET['value']);
                break;
            case "delta_temperature":
                $temp->setDeltaTemperature($_GET['value']);
                break;
            case "clear_statistics":
                $stat->clearStatistic($_GET['value']);
                break;
            case "set_starttime":
                echo "set_starttime to " . $_GET['value'];
                $time->setStartTime($_GET['value']);
                break;
            case "set_endtime":
               echo "set_endtime to " . $_GET['value'];
                $time->setEndTime($_GET['value']);
                break;
        }
        
    }else{
        switch($_GET['action']){
            case "temperature":
                echo number_format($temp->getTemperatureWithDelta(),1);
                break;
            case "heating":
                if($temp->isHeating()){
                    echo '<span class="glyphicon glyphicon-fire heating-on" aria-hidden="true"></span>';
                }else{
                    echo '<span class="glyphicon glyphicon-fire heating-off label-material-blue-30" aria-hidden="true"></span>';
                }
                break;
            case "stats":
                if(isset($_GET['from']) && !empty($_GET['from'])){
                    
                    $statsArray = array();
                    
                    if(isset($_GET['to']) && !empty($_GET['to'])){
                        $statistics = $stat->getStats($_GET['from'], $_GET['to']);
                    }else{
                        $statistics = $stat->getStats($_GET['from']);
                    }
                    
                    foreach($statistics as $key => $value){
                        $date = (date_parse($value['date']));
                        $statsArray[] = array("hour" => $date['year'] . "-" . $date['month'] . "-" . $date['day'] . " " . $date['hour'] . ":" . $date['minute'] . ":" . $date['second'], "value" => $value['temperature']+ $value['delta_temperature'], "heating" => ($value['heating']==1?"30":"0"));
                    }
                    echo json_encode($statsArray);
                }
                break;
        }
    }
}


?>