<!DOCTYPE html>
<html lang="de">
  <head>
    <?php echo file_get_contents("head.html"); ?>
  </head>
  <body>

  
  
  
    <div class="container">
        
        <?php echo file_get_contents("menu.html"); ?>
        
        <div class="well well-lg">

                <?php 
                  switch($_GET['page']){
                  
                    case "temperature": 
                        include("temperature.phtml");
                        break;
                    case  "statistic":
                        include("statistic.phtml");
                        break;
                    case "config":
                        include("config.phtml");
                        break;
                    default:
                        header("Location: ?page=temperature");
                  }
                  ?>
                  
                  
            </div>
        </div>
        
        
    </div>
    
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/date.js"></script>
    <script src="js/jquery.knob.min.js"></script>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script>
        $(document).ready(function() {
            $.material.init();
            $.material.ripples();
        });
    </script>
        
    
    <script src="js/custom.js"></script>
    
  </body>
</html>